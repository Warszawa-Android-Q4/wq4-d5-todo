package com.akademiakodu.todolist;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TodoListFragment extends Fragment implements TodoItemAdapter.OnTaskClickListener {
    @BindView(R.id.todo_list)
    protected RecyclerView mTodoList;

    private TodoItemAdapter mAdapter;
    private TodoListMenuListener mMenuListener;
    private TodoItemsRepository mRepository;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TodoListMenuListener) {
            mMenuListener = (TodoListMenuListener) context;
        } else {
            throw new RuntimeException("Activity nie implementuje TodoListMenuListener !");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_todolist, container, false);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);

        mTodoList.setLayoutManager(new LinearLayoutManager(getContext()));

        mRepository = new SqlTodoItemsRepository(getContext());

        mAdapter = new TodoItemAdapter(this);
        mAdapter.setData(mRepository.getTodoList());
        mTodoList.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_item_add) {
            mMenuListener.onAddClick();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTaskClick(TodoTask task) {
        mMenuListener.onItemClick(task);
    }

    @Override
    public void onTaskDoneChanged(TodoTask task, boolean done) {
        task.setDone(done);
        mRepository.save(task);
    }

    public interface TodoListMenuListener {
        void onAddClick();

        void onItemClick(TodoTask task);
    }
}












