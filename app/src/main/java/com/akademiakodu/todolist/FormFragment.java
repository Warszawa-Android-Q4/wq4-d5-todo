package com.akademiakodu.todolist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FormFragment extends Fragment {
    @BindView(R.id.task_title)
    protected EditText mTitle;
    @BindView(R.id.task_note)
    protected EditText mNote;
    @BindView(R.id.task_priority)
    protected CheckBox mPriority;

    private TodoTask mItem;
    private TodoItemsRepository mRepository;

    public static FormFragment editForm(TodoTask task) {
        FormFragment fragment = new FormFragment();
        Bundle args = new Bundle();

        args.putInt("id", task.getId());
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRepository = new SqlTodoItemsRepository(getContext());
        if (getArguments() != null) {
            int objId = getArguments().getInt("id");
            mItem = mRepository.findById(objId);
        }
        if (mItem == null) {
            mItem = new TodoTask();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form, container, false);
        ButterKnife.bind(this, view);

        mTitle.setText(mItem.getTitle());
        mNote.setText(mItem.getNote());
        mPriority.setChecked(mItem.isPriority());

        return view;
    }

    @OnClick(R.id.save)
    protected void onSaveClick() {
        mItem.setTitle(mTitle.getText().toString());
        mItem.setNote(mNote.getText().toString());
        mItem.setPriority(mPriority.isChecked());
        mItem.setCreated(new Date());

        mRepository.save(mItem);

        getActivity().getSupportFragmentManager().popBackStack();
    }
}
