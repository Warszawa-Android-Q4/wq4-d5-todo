package com.akademiakodu.todolist;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements TodoListFragment.TodoListMenuListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new TodoListFragment())
                    .commit();
        }
    }

    @Override
    public void onAddClick() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new FormFragment())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onItemClick(TodoTask task) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, FormFragment.editForm(task))
                .addToBackStack(null)
                .commit();
    }
}
